# poke-scripts
Scripts para calcular varios datos relacionados con Pokémon.

- **pkex.py**: calculador de experiencia.
- **pkstatus.py**: calculador de estadísticas.
- **rse_contets_pokeblocks.py**: calculador de Pokécubos para RZE.
