#!/usr/bin/env python3

FLAVOR_SPICY = 0
FLAVOR_DRY = 1
FLAVOR_SWEET = 2
FLAVOR_BITTER = 3
FLAVOR_SOUR = 4
SMOOTHNESS = 5

PETAYA_BERRY = (4, 0, -4, 4, -4, 80)
LIECHI_BERRY = (4, -4, 4, -1, -3, 80)
STARF_BERRY = (30, 10, 30, 10, 30, 30)


def main(argv=None):
    rse_contest_pokeblocs()


def rse_contest_pokeblocs():
    berries = [STARF_BERRY, STARF_BERRY, STARF_BERRY, STARF_BERRY]
    max_rpm = 100
    pokeblock = calculate_pokeblock(berries, max_rpm)
    print("Flavors: {0}".format(pokeblock[0]))
    print("Level: {0}".format(pokeblock[1]))
    print("Feel: {0}".format(pokeblock[2]))


def calculate_pokeblock(berries, max_rpm):
    num_berries = len(berries)
    flavors = [0, 0, 0, 0, 0]

    # Flavor calculation
    # First, sum the flavors and multiply by 10
    for berry in berries:
        flavors[FLAVOR_SPICY] += berry[FLAVOR_SPICY] * 10
        flavors[FLAVOR_DRY] += berry[FLAVOR_DRY] * 10
        flavors[FLAVOR_SWEET] += berry[FLAVOR_SWEET] * 10
        flavors[FLAVOR_BITTER] += berry[FLAVOR_BITTER] * 10
        flavors[FLAVOR_SOUR] += berry[FLAVOR_SOUR] * 10

    # Then, for each negative flavor, substract 1 to all flavors
    for flavor in flavors:
        if flavor < 0:
            for i in range(len(flavors)):
                flavors[i] -= 1

    # Set any negative flavor to 0
    for i in range(len(flavors)):
        if flavors[i] < 0:
            flavors[i] = 0

    # x = max_rpm / 333 + 1 rounded to two decimals
    x = round(max_rpm / 333 + 1, 2)

    # Finally, multiply each flavor by x and round it to the nearest integer
    for i in range(len(flavors)):
        flavors[i] = round(flavors[i] * x)

    # Calculate level (strongest flavor)
    level = 0
    for flavor in flavors:
        if flavor > level:
            level = flavor

    # Calculate feel
    feel = 0
    for berry in berries:
        feel += berry[SMOOTHNESS]
    feel = feel / num_berries - num_berries
    if feel > 99:
        feel = 99

    return (flavors, level, feel)


if __name__ == "__main__":
    main()
