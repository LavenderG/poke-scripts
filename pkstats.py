#!/usr/bin/env python3
# coding=UTF-8

import signal
import sys
from math import floor

""" Calculates a Pokémon stats """


def sigint_handler(signum, frame):
    print("")
    sys.exit(0)

# Calculates HP stat


def hp_stats(base, iv, ev, level, nature=None):
    return floor((((2 * base + iv + ev / 4) * level) / 100) + level + 10)

# Calculates remaining stats


def other_stats(base, iv, ev, level, nature):
    return floor(((((2 * base + iv + ev / 4) * level) / 100) + 5) * nature)


PROMPT = ">>"
STATS = {"HP": hp_stats, "OTHER": other_stats}


def main(argv=None):
    # Set up signal and execute program
    signal.signal(signal.SIGINT, sigint_handler)
    print_help()
    pkstats()


def pkstats():
    try:
        stat = input(PROMPT).upper()
    except EOFError:
        print("")
        sys.exit(0)

    if stat == "EXIT":
        print("")
        sys.exit(0)

    if stat in STATS:
        print("Chosen stat type \"{0}\"".format(stat))
        print("Input base stat:")
        base = ask_number()
        print("Input Pokémon stat IV:")
        iv = ask_number()
        print("Input Pokémon stat EV:")
        ev = ask_number()
        print("Input Pokémon level:")
        level = ask_number()
        print("Input Pokémon nature:")
        nature = ask_nature()

        calculated_stat = STATS[stat](base, iv, ev, level, nature)
        print("Stat will be: {0}".format(calculated_stat))
        print("\n")
        pkstats()
    else:
        print("Error: \"{0}\" is not a valid stat.".format(stat))
        pkstats()

# Prints program help


def print_help():
    print("Pokémon simple stat calculator")
    print("Enter EXIT to close the program or choose one of the following stats to start calculation: ")
    for stat in STATS:
        print("{0} ".format(stat),)
    print("")

# Asks until a number is provided


def ask_number():
    valid_number = False
    number = None
    while not valid_number:
        try:
            number = input(PROMPT)
        except EOFError:
            print("")
            sys.exit(0)
        if number.isdigit():
            number = int(number)
            valid_number = True
        else:
            print(
                "Error: \"{0}\" is not a valid number. Please input a valid number.".format(number))
    return number

# Asks until a nature is provided


def ask_nature():
    natures = {"HIGH": 1.1, "LOW": 0.9, "NEUTRAL": 1.0}
    valid_nature = False
    nature = None
    while not valid_nature:
        nature = input(PROMPT).upper()
        if nature in natures:
            nature = natures[nature]
            valid_nature = True
        else:
            print(
                "Error: \"{0}\" is not a valid nature. Please input a valid nature (HIGH, NEUTRAL or LOW).".format(nature))
    return nature


if __name__ == "__main__":
    sys.exit(main())
