#!/usr/bin/env python3
# coding=UTF-8

import signal
import sys

""" Calculates a Pokémon needed experience based on its level and experience type """


def sigint_handler(signum, frame):
    print("")
    sys.exit(0)

# Erratic experience type


def erratic(n):
    if n <= 50:
        return round((n**3 * (100 - n)) / 50)
    elif n >= 50 and n <= 68:
        return round((n**3 * (150 - n)) / 100)
    elif n >= 68 and n <= 98:
        return round((n**3 * ((1911 - 10 * n) / 3)) / 500)
    elif n >= 98 and n <= 100:
        return round((n**3 * (160 - n)) / 100)

# Fast experience type


def fast(n):
    return round((4 * n**3) / 5)

# Medium fast experience type


def med_fast(n):
    return n**3

# Medium slow experience type


def med_slow(n):
    return round((6 / 5) * n**3 - 15 * n**2 + 100 * n - 140)

# Slow experience type


def slow(n):
    return round((5 * n**3) / 4)

# Fluctiating experience type


def fluctuating(n):
    if n <= 15:
        return round(n**3 * (((n + 1) / 3 + 24) / 50))
    elif n >= 15 and n <= 36:
        return round(n**3 * ((n + 14) / 50))
    elif n >= 36 and n <= 100:
        return round(n**3 * ((n / 2 + 32) / 50))


PROMPT = ">>"

# Experience type names and associated functions
EXPERIENCE_TYPES = {"ERRATIC": erratic, "FAST": fast, "MEDIUM FAST": med_fast,
                    "MEDIUM SLOW": med_slow, "SLOW": slow, "FLUCTUATING": fluctuating}


def main(argv=None):
    # Setup signal, print help and start program
    signal.signal(signal.SIGINT, sigint_handler)
    print_help()
    pkexp()


def pkexp():
    exp_type = input(PROMPT).upper()

    if exp_type in EXPERIENCE_TYPES:
        print("Chosen experience type: \"{0}\"".format(exp_type))
        print("Input the first level:")
        start_level = ask_level()
        print("Input the second level:")
        end_level = ask_level()

        necessary_exp = EXPERIENCE_TYPES[exp_type](
            end_level) - EXPERIENCE_TYPES[exp_type](start_level)
        print("Needed experience for experience type {0} in level range {1}-{2}: {3}".format(
            exp_type, start_level, end_level, necessary_exp))
    else:
        print("Error: \"{0}\" is not a valid experience type. Input a valid experience type".format(
            exp_type))
        pkexp()

# Prints help


def print_help():
    print("Calculate a Pokémon needed experience based on its level and experience type.\n")
    print("Choose one of the following experience types: ")
    for exp_type in EXPERIENCE_TYPES:
        print("{0} ".format(exp_type))
    print("")


def ask_level():
    valid_level = False
    level = None
    while not valid_level:
        level = input(PROMPT)
        if level.isdigit():
            level = int(level)
            if level >= 0 and level <= 100:
                valid_level = True
            else:
                print(
                    "Error: \"{0}\" is not a valid number. Please input a number between 0 and 100.".format(level))
        else:
            print(
                "Error: \"{0}\" is not a valid number. Please input a number between 0 and 100.".format(level))
    return level


if __name__ == "__main__":
    sys.exit(main())
